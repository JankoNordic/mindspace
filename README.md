# Info
A 3D mind map creating tool implemented by using Three.js, React and Material-ui

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Run program locally
- npm install
- npm start

# Heroku link
https://mind-space-lite.herokuapp.com/

