import React, { Component } from "react";
import 'typeface-roboto';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import TimelineIcon from '@material-ui/icons/Timeline';
import TextFormatIcon from '@material-ui/icons/TextFormat';
import Tooltip from '@material-ui/core/Tooltip';
import { getPosition, getStyleValue} from './utility.js';
import './index.css';

class ObjectManipulationMenu extends Component
{
	divID = "objectManipulationMenu";

	constructor( props ) 
	{
		super( props );

		this.offSetX = getStyleValue( "div", this.divID, "left" );
		this.offSetY = getStyleValue( "div", this.divID, "top" );
		this.positionX = this.props.mouseAbsPos.x + this.offSetX;
		this.positionY = this.props.mouseAbsPos.y + this.offSetY;
	}

	shouldComponentUpdate( nextState ) 
	{
		if( this.props !== nextState )
		{
			let pos = getPosition( nextState.mouseAbsPos.x, 
								   nextState.mouseAbsPos.y, 
								   this.offSetX, 
								   this.offSetY, 
								   this.divElement.clientWidth, 
								   this.divElement.clientHeight );
			
			this.positionX = pos.x + this.offSetX;
			this.positionY = pos.y + this.offSetY;;

			return true;
		} else {

			return false;
		}
	}

	render() 
	{
		return (
		<div id = { this.divID } 
		ref={ ( divElement ) => { this.divElement = divElement } } 
		style={{ top : this.positionY, left : this.positionX } }
		onMouseOver = { ()=> this.props.clickMenuHovered( true ) } 
		onMouseOut = { ()=> this.props.clickMenuHovered( false ) }>

			<Tooltip title = "Draw a line between two items">
				<Fab id = "addLineButton" size= "small" 
				onClick={ this.props.onLineAddClicked }>
					<TimelineIcon />
				</Fab>
			</Tooltip>

			<Tooltip title = "Add item">
				<Fab id = "addItemButton" size= "small"
				onClick={ this.props.onAddClicked }>
					<AddIcon />
				</Fab>
			</Tooltip>

			<Tooltip title = "Edit text">
				<Fab id = "editTextButton" size= "small"
				onClick={ this.props.onTextEditClicked }>
					<TextFormatIcon />
				</Fab>
			</Tooltip>

			<Tooltip title = "Remove item">
				<Fab id = "remItemButton" size= "small"
				onClick={ this.props.onDelClicked }>
					<RemoveIcon />
				</Fab>
			</Tooltip>

			<Tooltip title = "Edit a place of item">
				<Fab id = "editPlaceButton" size= "small"
				onClick={ this.props.onCoordinatesFormClicked }>
					<LocationOnIcon />
				</Fab>
			</Tooltip>
		</div>
		);
	}
}

export default ObjectManipulationMenu;