
export function getPosition(positionX, positionY, offSetX, offSetY, elementWidth, elementHeight)
{
    let width = window.innerWidth;
    let height = window.innerHeight;

    let pos =
    {
    'x' : positionX , 'y' : positionY
    }

    if( -offSetX > positionX )
    {
        pos.x = -offSetX;

    } else if( positionX >= ( width - elementWidth - offSetX )){

        pos.x = ( width - elementWidth - offSetX);
    }

    if( -offSetY > positionY )
    {
        pos.y = -offSetY;

    } else if( positionY >= ( height - elementHeight - offSetY )){

        pos.y = ( height - elementHeight - offSetY );
    }
    
    return pos;
}

export function getStyleValue(type, id, attribute)
{
    let p = document.createElement(type);

    p.setAttribute("id", id);
    p.style.visibility = "hidden";

    document.body.appendChild(p);

    let value = getComputedStyle(p)[attribute];
    value = parseInt(value, 10);

    document.body.removeChild(p);

    return value;
}

export default {getStyleValue, getPosition}

