import React from "react";
import ReactDOM from "react-dom";
import MindMap from "./MindMap";
import "./index.css";
 
ReactDOM.render(
  <MindMap />, 
  document.getElementById("root")
);