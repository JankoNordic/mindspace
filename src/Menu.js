import React from 'react';
import Button from '@material-ui/core/Button';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TextField from '@material-ui/core/TextField';
import SaveIcon from '@material-ui/icons/Save';
import Fab from '@material-ui/core/Fab';
import './index.css';

let fineName;

class ControlledExpansionPanels extends React.Component {
  state = {
    expanded: null,
  };

  handleChange = panel => ( event, expanded ) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  saveName( event )
  {
    fineName = event.target.value;
  }

  render() {
    return (
      <div  id = "menu" 
      onMouseOver = { ()=> this.props.onSettingsHovered( true ) } 
      onMouseOut = { ()=> this.props.onSettingsHovered( false ) }>

          <ExpansionPanel expanded = { false }>
            <ExpansionPanelSummary className = "summaryPanel">
              <Typography> Save </Typography>
              <div id = "textField1"> <TextField variant = "standard" inputProps = {{ onChange : this.saveName }} /> </div>
              <Fab id = "button1" color = "default" size = "small" onClick = { ()=> this.props.save( fineName )}>
                <SaveIcon />
              </Fab>
            </ExpansionPanelSummary>
          </ExpansionPanel>

          <ExpansionPanel expanded = { false }>
            <ExpansionPanelSummary className = "summaryPanel">
              <Typography> Load </Typography>
              <div id = "filesContainer"> <input type = "file" id= "files " name = "files[]" size = "1" multiple onChange = { this.props.load }/> </div>
            </ExpansionPanelSummary>
          </ExpansionPanel>


          <ExpansionPanel expanded = { false }>
            <ExpansionPanelSummary className = "summaryPanel">
            <div id = "cameraResetButton"> <Button variant="contained" onClick = { this.props.resetCamera }> Reset camera </Button> </div>
            </ExpansionPanelSummary>
          </ExpansionPanel>
      
          <ExpansionPanel>
          <ExpansionPanelSummary expandIcon = { <ExpandMoreIcon /> } className = "summaryPanel">
            <Typography id = "textField2"> Info </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography> A mind map creation tool created by using Three.js and React. </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </div>
    );
  }
}

export default ControlledExpansionPanels;