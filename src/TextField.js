import React, { Component } from "react";
import 'typeface-roboto';
import TextField from '@material-ui/core/TextField';
import { getPosition, getStyleValue } from './utility.js';
import './index.css';

class TextInput extends Component
{
	divID = "nameInput";

	constructor( props ) 
	{
		super( props );

		this.offSetX = getStyleValue( "div", this.divID, "left" );
		this.offSetY = getStyleValue( "div", this.divID, "top" );
		this.positionX = this.props.mouseAbsPos.x + this.offSetX;
		this.positionY = this.props.mouseAbsPos.y + this.offSetY;
	}

	shouldComponentUpdate( nextState ) 
	{
		if( this.props !== nextState )
		{
			let pos = getPosition( nextState.mouseAbsPos.x, 
								   nextState.mouseAbsPos.y, 
								   this.offSetX, this.offSetY, 
								   this.divElement.clientWidth, 
								   this.divElement.clientHeight );

			this.positionX = pos.x + this.offSetX;
			this.positionY = pos.y + this.offSetY;

			return true;
		} else {

			return false;
		}
	}
	
	render() 
	{
		return (
		<div id= { this.divID } ref = {( divElement ) => { this.divElement = divElement }} 
		style={{ top : this.positionY, left : this.positionX }} >

			<TextField variant="filled" autoFocus={ true } inputProps={{ onChange : this.props.onTextFieldClicked }} />
		</div>
		);
	}
}
export default TextInput;