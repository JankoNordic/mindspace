import React, { Component } from "react";
import 'typeface-roboto';
import { FormHelperText, Input } from '@material-ui/core';
import { getPosition, getStyleValue } from './utility.js';
import './index.css';

class CoordinatesForm extends Component
{
	divID = "coordinatesForm";

	constructor( props )
    {
	   super( props );

	   this.offSetX = getStyleValue("div", this.divID, "left");
	   this.offSetY = getStyleValue("div", this.divID, "top");
	   this.positionX = this.props.mouseAbsPos.x + this.offSetX;
	   this.positionY = this.props.mouseAbsPos.y + this.offSetY;

	   this.setCoordinateX = this.setCoordinateX.bind( this );
	   this.setCoordinateY = this.setCoordinateY.bind( this );
	   this.setCoordinateZ = this.setCoordinateZ.bind( this );
	   
	   this.deSelectX = this.deSelectX.bind( this );
	   this.deSelectY = this.deSelectY.bind( this );
	   this.deSelectZ = this.deSelectZ.bind( this );
	}

	componentDidMount() 
    {
		this.x = this.props.objectPos.x;
		this.y = this.props.objectPos.y;
		this.z = this.props.objectPos.z;
	}

	shouldComponentUpdate( nextState ) 
	{
		if( this.props !== nextState )
		{
			let pos = getPosition( nextState.mouseAbsPos.x, nextState.mouseAbsPos.y, this.offSetX, this.offSetY, this.divElement.clientWidth, this.divElement.clientHeight );
			this.positionX = pos.x + this.offSetX;
			this.positionY = pos.y + this.offSetY;

			return true;
		} else {

			return false;
		}
	}

	setCoordinateX( event )
	{
		this.x = event.target.value;
	}

	setCoordinateY( event )
	{
		this.y = event.target.value;
	}

	setCoordinateZ( event )
	{
		this.z = event.target.value;
	}

	deSelectX()
	{
		this.props.updateObjectPosition( 'x', this.x );
	}

	deSelectY()
	{
		this.props.updateObjectPosition( 'y', this.y );
	}

	deSelectZ()
	{
		this.props.updateObjectPosition( 'z', this.z );
	}

	render() 
	{
	    return (
		<div  id = { this.divID } style={{ top : this.positionY, left : this.positionX }} ref={ (divElement) => { this.divElement = divElement }}
		onMouseOver = { ()=>this.props.onCoordinatesFormHovered( true )} onMouseOut = { ()=>this.props.onCoordinatesFormHovered( false )}>
			
			<form autoComplete="off" >

				<FormHelperText id ="xInputHelp"> x </FormHelperText>
				<FormHelperText id ="yInputHelp"> y </FormHelperText>
				<FormHelperText id ="zInputHelp"> z </FormHelperText>

				<div><Input  id ="xInput" defaultValue={Math.round( this.props.objectPos.x * 100) / 100 } size="small" 
				inputProps={{ onChange : this.setCoordinateX, onBlur : this.deSelectX }} autoFocus={ true } /> </div>

				<div><Input  id ="yInput" defaultValue={Math.round( this.props.objectPos.y * 100) / 100 } size="small"  
				inputProps={{ onChange : this.setCoordinateY, onBlur : this.deSelectY }}/> </div>

				<div><Input  id ="zInput" defaultValue={Math.round( this.props.objectPos.z * 100) / 100 } size="small"  
				inputProps={{ onChange : this.setCoordinateZ, onBlur : this.deSelectZ }}/> </div>
			</form>
		</div>
        );
	}
}
export default CoordinatesForm;