import React, { Component } from "react";
import 'typeface-roboto';
import Fab from '@material-ui/core/Fab';
import MenuIcon from '@material-ui/icons/Menu';
import './index.css';


class MenuButton extends Component
{
	render() 
	{
	return (
	  <Fab id = "menuButton" color = "default" size = "small"
	  onClick = { this.props.onclickSettingsButton }
	  onMouseOver = { ()=> this.props.onSettingsButtonHovered( true ) }
	  onMouseOut = { ()=> this.props.onSettingsButtonHovered( false ) }>
	    <MenuIcon />
      </Fab>
		);
	}
}

export default MenuButton;