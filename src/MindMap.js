import React, { Component } from "react";
import * as THREE from "three";
import CameraControls from 'camera-controls';
import ObjectManipulationMenu from './ObjectManipulationMenu';
import TextInput from './TextField';
import CoordinatesForm from './CoordinatesForm';
import MenuButton from './MenuButton';
import ControlledExpansionPanels from './Menu';
import DragControls from 'three-dragcontrols';
import { saveAs } from 'file-saver';
import Helvetiker from './helvetiker_regular.typeface.json';
import Background from './Background.jpg';

CameraControls.install( { THREE: THREE } );

class MindMap extends Component {

    // Constants
    sectorLength = 100;

    // State variables
    hideElements = true;
    lineAdd; justDragged = false;
    justDragged = false;
    resetCameraClicked = false;

    // Show element selectors
    showCoordinatesForm = false;
    showMenu = false;
    showSettings = false;
    showTextField = false;

    // Hovered
    settingsButtonHovered = false;
    settingsHovered = false;
    dragObjectHovered = false;
    coordinatesFormHovered = false;
    clickMenuHovered = false;

    // Containers
    sectorArray = {};
    linesArray = {};
    linesObjectArray = [];
    sceneObjects = [];

    cameraAdjustment =
    {
      'start' : false , 'end' : false, 'updated' : false
    }

    // The rendering space is divided into smaller areas (cubes) to alleviate rendering load when numerous objects are in the rendering space
    cubeFactors = {1:[0,0,0], 2:[1,0,0], 3:[-1,0,0], 4:[0,0,1], 5:[0,0,-1], 6:[1,0,1], 7:[1,0,-1], 8:[-1,0,1], 9:[-1,0,-1], 
                   10:[0,1,0], 11:[1,1,0], 12:[-1,1,0], 13:[0,1,1], 14:[0,1,-1], 15:[1,1,1], 16:[1,1,-1], 17:[-1,1,1], 18:[-1,1,-1],
                   19:[0,-1,0], 20:[1,-1,0], 21:[-1,-1,0], 22:[0,-1,1], 23:[0,-1,-1], 24:[1,-1,1], 25:[1,-1,-1], 26:[-1,-1,1], 27:[-1,-1,-1]}

    constructor()
    {
       super();

      this.state =
      {
        mouseAbsPos: {x : 0 , y: 0},
      }

      this.coolDowns =
      {
        objectMenu : 0, textInput: 0, showCoordinatesForm: 0
      }
 
       // Create scene components
       this.raycaster = new THREE.Raycaster();
       this.ambient = new THREE.AmbientLight( 0xffffff, 0.1 );
       this.mouse = new THREE.Vector2(0, 0);
       this.mouseAbs = new THREE.Vector2(0, 0);
       this.scene = new THREE.Scene();
       this.spotLight = new THREE.SpotLight( 0xffffff, 2 );

        // Binds
        this.createText = this.createText.bind(this);
        this.createSpere = this.createSpere.bind(this);
        this.onTextEditClicked = this.onTextEditClicked.bind(this);
        this.onMouseClick = this.onMouseClick.bind(this);
        this.onAddClicked = this.onAddClicked.bind(this);
        this.chechElementPosition = this.chechElementPosition.bind(this);
        this.onTextFieldClicked = this.onTextFieldClicked.bind(this);
        this.updateLinePositions = this.updateLinePositions.bind(this);
        this.showElement = this.showElement.bind(this);
        this.onDelClicked = this.onDelClicked.bind(this);
        this.onCoordinatesFormClicked = this.onCoordinatesFormClicked.bind(this);
        this.onClickMenuHovered = this.onClickMenuHovered.bind(this);
        this.onCoordinatesFormHovered = this.onCoordinatesFormHovered.bind(this);
        this.updateObjectPosition = this.updateObjectPosition.bind(this);
        this.onLineAddClicked = this.onLineAddClicked.bind(this);
        this.attachLine = this.attachLine.bind(this);
        this.onclickSettingsButton = this.onclickSettingsButton.bind(this);
        this.onSettingsHovered = this.onSettingsHovered.bind(this);
        this.animateCamera = this.animateCamera.bind(this);
        this.onSettingsButtonHovered = this.onSettingsButtonHovered.bind(this);
        this.resetCamera = this.resetCamera.bind(this);
        this.save = this.save.bind(this);
        this.load = this.load.bind(this);
    }

    componentDidMount() 
    {
       let width = window.innerWidth;
       let height = window.innerHeight;

       this.camera = new THREE.PerspectiveCamera( 75, width / height, 0.01, 1000 );
       
       let gameCanvas = document.querySelector( '#canvasID' );
       this.renderer = new THREE.WebGLRenderer({ alpha: true, canvas: gameCanvas});
       this.renderer.setSize( width, height );
       this.mount.appendChild( this.renderer.domElement );

       this.dragControls = new DragControls( this.sceneObjects, this.camera, this.renderer.domElement );

       this.cameraControls = new CameraControls( this.camera, this.renderer.domElement );
       this.cameraControls.setTarget( 0, 0, 0 );
       this.cameraControls.setPosition( 10, 0, 10 );
       this.cameraControls.dollySpeed = 1.3;

       // Light
        this.spotLight.position.set( 100, 100, 100 );
        this.spotLight.angle = Math.PI / 2;
        this.spotLight.penumbra = 0.2;
        this.spotLight.decay = 1;
        this.spotLight.distance = 0;
        this.spotLight.castShadow = false;
        this.spotLight.shadow.mapSize.width = 1024;
        this.spotLight.shadow.mapSize.height = 1024;
        this.spotLight.shadow.camera.near = 10;
        this.spotLight.shadow.camera.far = 200;
        this.scene.add( this.spotLight );
        this.scene.add( this.ambient );

       // Background
        const loader = new THREE.TextureLoader();
        loader.load(Background, function(texture)
        {
            this.scene.background = texture; 

        }.bind(this));
    
       // Create initial scene objects
       let coordinates = { 'x' : 0, 'y' : 0, 'z' : 0 }
        for (let i = 0; i < 1; i++) 
        {
            coordinates.x = 0;
            for (let j = 0; j < 1; j++) 
            {
                coordinates.y = 0;
                for (let k = 0; k < 1; k++) 
                {
                    let sphere = this.createSpere(coordinates);
                    let textObj =this.createText("Core concept");

                    sphere.add(textObj);
                    this.sceneObjects.push(sphere);
                    this.scene.add(sphere);

                    coordinates.y = coordinates.y + 20;
                }
                coordinates.x = coordinates.x +20;
            }
            coordinates.z = coordinates.z + 20;
        }

        // Event listeners
        this.dragControls.addEventListener( 'dragstart', function ( event ) {
            
            this.cameraControls.enabled = false;
            this.showElement("all", false);

            if((typeof(this.selectedObject) !== 'undefined' ))
            {
                this.selectedObject.material.color.set( 0x350000 )
                this.previousSelectedObject = this.selectedObject;
            }

            this.selectedObject = event.object;
            event.object.material.color.set( 0x000000 )

        }.bind(this));

        this.dragControls.addEventListener( 'dragend', function ( event ) {

            this.cameraControls.enabled = true;
            this.updateLinePositions(event.object);

        }.bind(this));

        this.dragControls.addEventListener( 'dragstart', function ( event ) {

            this.cameraControls.enabled = false;

        }.bind(this));

        this.dragControls.addEventListener( 'hoveron', function ( event ) {

            this.dragObjectHovered = true;

        }.bind(this));
        
        this.dragControls.addEventListener( 'hoveroff', function ( event ) {

            this.dragObjectHovered = false;

        }.bind(this));

        this.dragControls.addEventListener( 'drag', function ( event ) {

            this.justDragged = true;

        }.bind(this));

       this.cameraControls.addEventListener( 'controlstart', function ( event ) {

            this.cameraAdjustment.end = false;
            this.cameraAdjustment.start = true;

        }.bind(this));

        this.cameraControls.addEventListener( 'control', function ( event ) {

            if(this.cameraAdjustment.start === true && this.cameraAdjustment.end === false)
            {
                this.cameraAdjustment.updated = true;
            }

        }.bind(this));

        // Rendering loops
        setInterval(this.chechElementPosition, 10);

        this.animateCamera();

        // Calculates re-rendered texts and defines render frequency
        let subSectorArray = {};
        let cameraPostionPrevious = new THREE.Vector3(0,0,0);
        let a = 0;
        setInterval(
            async function()
            {
            if( cameraPostionPrevious.x !== this.camera.position.x ||
                cameraPostionPrevious.y !== this.camera.position.y || 
                cameraPostionPrevious.z !== this.camera.position.z )
                {
                    a = 0;
                    subSectorArray = getRotable(this.camera.position, this.sectorLength, this.sectorArray, this.cubeFactors);
                    cameraPostionPrevious = new THREE.Vector3(this.camera.position.x, this.camera.position.y, this.camera.position.z);
                }
            }.bind(this), 100);

            setInterval(
                async function()
                {
                    for (let i = 1; i <= 30; i++) 
                    {
                        if(!(typeof(subSectorArray) === 'undefined' ) && a < subSectorArray.length)
                        {
                            subSectorArray[a].lookAt( this.camera.position );
                            a = a+1;
                        }
                    }
                }.bind(this), 1);

        window.addEventListener( 'resize', function ( event ) {

            let width = window.innerWidth;
            let height = window.innerHeight;

            if(this.windowWidth !== width || this.windowHeight !== height)
            {
                this.windowWidth = width;
                this.windowHeight = height;
                this.renderer.setSize( this.windowWidth, this.windowHeight );
                this.camera.aspect = (( this.windowWidth) / this.windowHeight );
                this.camera.position.set(5, 0, 5);
                this.camera.updateProjectionMatrix();
            }

        }.bind(this));
    }

    async animateCamera()
    {
       const clock = new THREE.Clock();
       let this1 = this;

       async function animate()
        {
            const delta = clock.getDelta();
            this1.cameraControls.update( delta );
            requestAnimationFrame( animate );
            this1.renderer.render(this1.scene, this1.camera);
        }

        animate();
    }

    // File managing and data loading and saving
    async save(filename) 
    {
        let json = this.createOutputJson();
        let blob = new Blob([json], {type: "application/json"});
        saveAs(blob, filename);
    }

    async load(event) 
    {

      let file = event.target.files[0]
      let reader= new FileReader();

      // Reset scene and variables
      for (let i = this.scene.children.length - 1; i >= 0; i--) 
      {
        if(this.scene.children[i].type === "Mesh" || this.scene.children[i].name === "line")
        {
            this.scene.remove(this.scene.children[i]);
        }
      }

      this.sectorArray.length = 0;
      this.linesArray.length = 0;
      this.linesObjectArray.length = 0;
      this.sceneObjects.length = 0;

      // Load new scene
      reader.onload = function(file)
      {
        try 
        {
            let json = reader.result;
            let jsonObject = JSON.parse(json);

            // Load spheres
            for (let i = 0; i < jsonObject.spheres.length; i++)
            {
                let textObj;
                let sphere = this.createSpere(jsonObject.spheres[i].position);
                this.sceneObjects.push(sphere);

                if(jsonObject.spheres[i].text !== null)
                {
                    textObj = this.createText(jsonObject.spheres[i].text);
                    sphere.add(textObj);
                }
                this.scene.add(sphere);

                // Replace line's old IDs with new IDs with - sign to avoid multiple replacements
                for (let j = 0; j < jsonObject.lines.length; j++)
                {
                    if(jsonObject.lines[j].start === jsonObject.spheres[i].id)
                    {
                        jsonObject.lines[j].start = -sphere.id;
                    }

                    if(jsonObject.lines[j].end === jsonObject.spheres[i].id)
                    {
                        jsonObject.lines[j].end = -sphere.id;
                    }
                }
            }

            // Get new IDs by multiplying with -1
            for (let i = 0; i < jsonObject.lines.length; i++)
            {
                jsonObject.lines[i].start = -jsonObject.lines[i].start;
                jsonObject.lines[i].end = -jsonObject.lines[i].end;
            }

            // Create lines
            for (let i = 0; i < jsonObject.lines.length; i++)
            {
                // start
                let startObject;
                for ( let j = 0; j < this.sceneObjects.length; j++ )
                {
                    if( this.sceneObjects[j].id === jsonObject.lines[i].start )
                    {
                        startObject = this.sceneObjects[j];
                        break;
                    }
                }
                
                // end
                let endObject;
                for ( let j = 0; j < this.sceneObjects.length; j++ )
                {
                    if( this.sceneObjects[j].id === jsonObject.lines[i].end )
                    {
                        endObject = this.sceneObjects[j];
                        break;
                    }
                }

                let line = this.createLine(startObject.position, endObject.position);
                this.scene.add(line);
                this.attachLine(startObject, endObject, line);
            }
        } catch (e) {
            return false;
        }
      }.bind(this);
      reader.readAsText(file);
      
    }

    createOutputJson() {
        let jsonObject = {spheres: 0, lines: 0};
        let spheres = [];

        for (let i = 0; i < this.sceneObjects.length; i++) 
        {
           let cell = {
               id : this.sceneObjects[i].id, position: this.sceneObjects[i].position, 
               text : this.sceneObjects[i].children.length > 0 ? this.sceneObjects[i].children[0].name : null
           }
           spheres.push(cell);
        }

        jsonObject.spheres = spheres;
        jsonObject.lines = this.linesObjectArray;
        let json = JSON.stringify(jsonObject, null, 2);
        return(json);
    }

    // Changes elements visibility
    async showElement(element, visibility) 
    {
        if(element === "objectMenu")
        {
            if(visibility === false)
            {
                this.showMenu = false;
                this.coolDowns.objectMenu = (new Date()).getTime();
            }
            else if(visibility === true && ((new Date()).getTime() - this.coolDowns.objectMenu > 0.1))
            {
                this.showMenu = true;
            }
        }
       else if(element === "settings")
        {
            if(visibility === false)
            {
                this.showSettings = false;
            }
            else if(visibility === true)
            {
                this.showSettings = true;
            }
        }
        else if(element === "textInput")
        {
            if(visibility === false)
            {
                this.showTextField = false;
                this.coolDowns.textInput = (new Date()).getTime();
            }
            else if(visibility === true && ((new Date()).getTime() - this.coolDowns.textInput > 0.1))
            {
                this.showTextField = true;
            }
        }
        else if(element === "coordinatesForm")
        {
            if(visibility === false)
            {
                this.showCoordinatesForm = false;
                this.coolDowns.showCoordinatesForm = (new Date()).getTime();
            }
            else if(visibility === true && ((new Date()).getTime() - this.coolDowns.showCoordinatesForm > 0.1))
            {
                this.showCoordinatesForm = true;
            }
        }

        else if(element === "all")
        {
            if(visibility === false)
            {
                this.showTextField = false;
                this.showMenu = false;
                this.showCoordinatesForm = false;
                this.showSettings = false;
                
                this.coolDowns.objectMenu = (new Date()).getTime();
                this.coolDowns.showCoordinatesForm = (new Date()).getTime();
                this.coolDowns.textInput = (new Date()).getTime();
            }
        }
        this.forceUpdate();
    }

    // Hovering
    async onClickMenuHovered(value)
    {
        this.clickMenuHovered = value;
    }

    async onSettingsHovered(value)
    {
        this.settingsHovered = value;
    }

    async onCoordinatesFormHovered(value)
    {
        this.coordinatesFormHovered = value;
    }

    async onSettingsButtonHovered(value)
    {
        this.settingsButtonHovered = value;
    }
    
    // Clicks
    async onclickSettingsButton()
    {
        this.showSettings ? this.showElement("settings", false) : this.showElement("settings", true);
    }

    async onTextEditClicked() 
    {
        this.chainedElementAdded = true;
        this.showElement("objectMenu", false);
        this.showElement("textInput", true);
    }

    async onTextFieldClicked(event) 
    {
        if(!(typeof(event) === 'undefined' ))
		{
            this.selectedObject.remove(...this.selectedObject.children);
            let textObj = this.createText(event.target.value);
            this.selectedObject.add(textObj);
		}
    }

    async onCoordinatesFormClicked()
    {
        this.chainedElementAdded = true;
        this.showElement("objectMenu", false);
        this.showElement("coordinatesForm", true);
    }

    async onLineAddClicked()
    {
        this.lineStart = this.selectedObject;
        this.lineAdd = true;
    }

    async onAddClicked() 
    {
        this.showElement("objectMenu", false);

        let coordinates = new THREE.Vector3(this.selectedObject.position.x, this.selectedObject.position.y+5, this.selectedObject.position.z);
        let sphere = this.createSpere(coordinates);
        this.scene.add(sphere);
        this.sceneObjects.push(sphere);

        this.linesArray[sphere] = [];
        let line = this.createLine(new THREE.Vector3(coordinates.x, coordinates.y-5, coordinates.z), coordinates);
        this.scene.add( line );
        this.attachLine(this.selectedObject, sphere, line);

        this.selectedObject.material.color.set( 0x350000 );
        this.selectedObject = sphere;
        this.selectedObject.material.color.set( 0x000000 );

        this.showElement("textInput", true);
        this.chainedElementAdded = true;
    }

    async onDelClicked() 
    {
        this.showElement("objectMenu", false);

        // Remove line(s) from scene
        if(typeof(this.selectedObject) !== 'undefined' && this.selectedObject.id in this.linesArray)
        {
             let lines = this.linesArray[this.selectedObject.id];
             let i = 0;

             for (i = 0; i < lines.length; i++) 
             {
                this.scene.remove(lines[i].line);
             }
        }

        // Remove line object(s)
        for (let i = this.linesObjectArray.length- 1; i >= 0; i--) 
        {
           if(this.linesObjectArray[i].end === this.selectedObject.id ||
              this.linesObjectArray[i].start === this.selectedObject.id )
           {
              this.linesObjectArray.splice(i,1);
           }
        }

        // Remove sphere object
        for (let i = this.sceneObjects.length- 1; i >= 0; i--) 
        {
           if(this.sceneObjects[i].id === this.selectedObject.id)
           {
            this.sceneObjects.splice(i,1);
           }
        }

        // Remove line from linesArray
        delete this.linesArray[this.selectedObject.id];

        // Remove sphere from scene
        this.scene.remove(this.selectedObject);
        this.selectedObject = undefined;
    }

    async onMouseClick(event) 
    {
        this.hideElementsUpdate();
        this.chechElementPosition();

        this.cameraAdjustment.end = true;
        let allLines = true;
        let mouse = new THREE.Vector2(0,0);

        mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

        this.raycaster.setFromCamera( mouse, this.camera );
        let intersects = this.raycaster.intersectObjects( this.scene.children );

        for ( let i = 0; i < intersects.length; i++ ) 
        {
            if(intersects[ i ].object.name !== "line")
            {
               allLines = false;
            }
        }

        // Add new line
        if(this.lineAdd && !this.clickMenuHovered)
        {
            if(this.selectedObject !== this.previousSelectedObject)
            {
                let line = this.createLine(this.previousSelectedObject.position, this.selectedObject.position);
                this.attachLine(this.previousSelectedObject, this.selectedObject, line);
                this.scene.add( line );
                this.lineAdd = false;
            }
        }

        // Show object menu
        if(!allLines && !this.lineAdd && !this.clickMenuHovered)
        {
            if(this.dragObjectHovered  && !this.justDragged)
            {
                this.showElement("objectMenu", true);
                this.showElement("textInput", false);
                this.showElement("coordinatesForm", false);
            }
        }

        if(this.hideElements)
        {
            this.showElement("all", false);
        }

       this.cameraAdjustment.start = false;
       this.cameraAdjustment.updated = false;
       this.justDragged = false;
       this.clickMenuHovered = false;
       this.chainedElementAdded = false;
       this.hideElements = true;
   }

    // Element creation
    createText(text) 
    {
        let  loader = new THREE.FontLoader();
        let font = loader.parse( Helvetiker );
        let textOriginal = text;
        text = '  '.concat(text);

        let material = new THREE.MeshBasicMaterial( { color: 0x000000 } );
        let factor = 0.01;
        let geometry = new THREE.TextBufferGeometry(text, {
          font: font,
          size: 80*factor,
          height: 0,
          curveSegments: 4,
          bevelEnabled: false,
          bevelThickness: 0,
          bevelSize: 0,
          bevelOffset: 0,
          bevelSegments: 1
        });
        
        let textObj = new THREE.Mesh( geometry, material );
        textObj.name = textOriginal;
        textObj.lookAt( this.camera.position );
        textObj.position.y = textObj.position.y-0.4;

        let fullKey = parseKey(textObj.position, this.sectorLength);
                 
        if(!(fullKey in this.sectorArray))
        {
            this.sectorArray[fullKey] = [];
            this.sectorArray[fullKey].push(textObj);
        } else {
            this.sectorArray[fullKey].push(textObj);
        }
        
        return textObj;
    }

    createSpere(coordinates) 
    {
        let sphereInit = new THREE.SphereBufferGeometry( 0.4, 32, 32 );
        let material = new THREE.MeshPhongMaterial( { color: 0x350000, dithering: true } );
        let sphere = new THREE.Mesh( sphereInit, material );

        sphere.position.set(coordinates.x, coordinates.y, coordinates.z);

        return sphere;
    }

    createLine(start, end)
    {
        let material = new THREE.LineBasicMaterial({
            color: 0x000000
        });
        
        let geometry = new THREE.Geometry();
        geometry.vertices.push(
            new THREE.Vector3(start.x, start.y, start.z),
            new THREE.Vector3(end.x, end.y, end.z),
        );
        
        let line = new THREE.Line( geometry, material );
        line.name = "line";

        return line;
    }

    async attachLine(startObject, endObject, line) 
    {
        this.showElement("objectMenu", false);

        let element = {start: startObject.id, end: endObject.id}
        this.linesObjectArray.push(element);
        
        if(startObject.id in this.linesArray)
        {
            this.linesArray[startObject.id].push({line : line, point : 0});
        } else {
            this.linesArray[startObject.id] = [];
            this.linesArray[startObject.id].push({line : line, point : 0});
        }

        if(endObject.id in this.linesArray)
        {
            this.linesArray[endObject.id].push({line : line, point : 1});
        } else {
            this.linesArray[endObject.id] = [];
            this.linesArray[endObject.id].push({line : line, point : 1});
        }
    }

   // Updates
   hideElementsUpdate()
   {
       if( this.cameraAdjustment.updated || this.settingsButtonHovered ||
           this.settingsHovered || this.coordinatesFormHovered || 
           this.dragObjectHovered || this.clickMenuHovered || 
           this.chainedElementAdded )
       {
           this.hideElements = false;
       }
       
       if((!this.chainedElementAdded && this.dragObjectHovered && this.clickMenuHovered) || this.lineAdd)
       {
           this.hideElements = true;
       }
   }

   async updateObjectPosition(axis, value) 
   {
       if(!isNaN(value))
       {
            this.chainedElementAdded = true;
            if(axis === "x")
            {
                    this.selectedObject.position.x = value;
            } else if (axis === "y")
            {
                    this.selectedObject.position.y = value;
            } else if (axis === "z")
            {
                    this.selectedObject.position.z = value;
            }
            this.updateLinePositions(this.selectedObject);
       }
   }

   async updateLinePositions(object) 
   {
       let lines;

       if(typeof(object) !== 'undefined' && object.id in this.linesArray)
       {
            lines = this.linesArray[object.id];

            let i = 0;
            for (i = 0; i < lines.length; i++) 
            {
                if(lines[i].point === 0)
                {
                    lines[i].line.geometry.vertices[0] = object.position;
                    lines[i].line.geometry.verticesNeedUpdate = true;
                }
      
                if(lines[i].point === 1)
                {
                    lines[i].line.geometry.vertices[1] = object.position;
                    lines[i].line.geometry.verticesNeedUpdate = true;
                }
            }
       }
   }

   async chechElementPosition() 
   {    
        if((typeof(this.selectedObject) !== 'undefined'))
        {
            let result = positionOnTheBrowser(this.selectedObject.position, this.camera, this.mount.clientWidth, this.mount.clientHeight);
            if((typeof(result)  !== 'undefined'))
            {
                let posx = document.querySelector('#canvasID').getBoundingClientRect().left
                let posy = document.querySelector('#canvasID').getBoundingClientRect().top

                this.setState({
                    mouseAbsPos: {x :(result.x +posx), y: (result.y+posy)}
                });
            }
        }    
    }

   async resetCamera() 
   { 
        if(!this.resetCameraClicked)
        {
            this.cameraControls.setPosition( 10, 0, 10 );
            this.cameraControls.setTarget( 0, 0, 0 );
        }
        this.resetCameraClicked = !this.resetCameraClicked ;
    }

   render() 
   {
      return (
      <div id = "rootElement" ref = { ref => ( this.mount = ref )} onClick = { this.onMouseClick }>

        <canvas id = "canvasID" > </canvas>

        <div id ="menuButton"> 
            <MenuButton 
            onclickSettingsButton = { this.onclickSettingsButton } 
            onSettingsButtonHovered = { this.onSettingsButtonHovered }> 
            </MenuButton> 
        </div>

        <div id ="ControlledExpansionPanels"> { this.showSettings ? 
            <ControlledExpansionPanels 
            onSettingsHovered = { this.onSettingsHovered } 
            save = { this.save } 
            load = { this.load }  
            resetCamera = { this.resetCamera }>> 
            </ControlledExpansionPanels> : null } 
        </div>

        <div id ="ObjectManipulationMenu1"> { this.showMenu ? 
            <ObjectManipulationMenu 
            clickMenuHovered = { this.onClickMenuHovered } 
            mouseAbsPos = { this.state.mouseAbsPos } 
            onTextEditClicked = { this.onTextEditClicked } 
            onAddClicked = { this.onAddClicked } 
            onDelClicked = { this.onDelClicked }
            onCoordinatesFormClicked = { this.onCoordinatesFormClicked }
            onLineAddClicked = { this.onLineAddClicked }>
            </ObjectManipulationMenu> : null } 
        </div>

        <div id ="textField"> { this.showTextField ? 
            <TextInput 
            mouseAbsPos = { this.state.mouseAbsPos }
            onTextFieldClicked = { this.onTextFieldClicked } >>
            </TextInput> : null } 
        </div>

        <div id ="coordinatesForm"> { this.showCoordinatesForm ? 
            <CoordinatesForm
            onCoordinatesFormHovered = { this.onCoordinatesFormHovered }
            mouseAbsPos = { this.state.mouseAbsPos }
            objectPos = { this.selectedObject.position }
            updateObjectPosition = { this.updateObjectPosition }>
            </CoordinatesForm> : null } 
        </div>

      </div>
        )
    }
}

// Help functions
function parseKey(position, sectorLength)
{
    let keyX = Math.floor(position.x/sectorLength);
    let keyY = Math.floor(position.y/sectorLength);
    let keyZ = Math.floor(position.z/sectorLength);

    keyX = keyX.toString();
    keyY = keyY.toString();
    keyZ = keyZ.toString();

    let fullKey = ''.concat(keyX, keyY, keyZ);

    return fullKey;
}

function positionOnTheBrowser(position, camera, width, height) 
{
    if(typeof(position) !== "undefined")
    {
        let p = new THREE.Vector3(position.x, position.y, position.z);
        let vector = p.project(camera);
        
        vector.x = (vector.x + 1) / 2 * width ;
        vector.y = -(vector.y - 1) / 2 * height;
        
        return vector;
    }
}

function getRotable(position, sectorLength, sectorArray, cubeFactors)
{
    let subSectorArray =  [];
    let subSectorArrayTemp =  [];
    let x = position.x;
    let y = position.y;
    let z = position.z;
    
    for (let i = 1; i <= Object.keys(cubeFactors).length; i++) 
    {
        let adjustedposition = new THREE.Vector3(x,y,z);
        adjustedposition.x = (x + sectorLength*cubeFactors[i][0]);
        adjustedposition.y = (y + sectorLength*cubeFactors[i][1]);
        adjustedposition.z = (z + sectorLength*cubeFactors[i][2]);

        let fullKey = parseKey(adjustedposition, sectorLength);
        subSectorArrayTemp = sectorArray[fullKey];

        if((typeof(subSectorArrayTemp) !== 'undefined' ))
        {
            subSectorArray.push(...subSectorArrayTemp)
        }
    }
    return subSectorArray;
}

export default MindMap;