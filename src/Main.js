import React, { Component } from "react";
import {
  Route,
  HashRouter
} from "react-router-dom";

import MindMap from "./MindMap";
 
class Main extends Component {
  render() {
    return (
        <HashRouter>
        <Route exact path="/" component={MindMap}/> 
        </HashRouter>
    );
  }
}
 
export default Main;